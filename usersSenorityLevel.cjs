let givenData=require('./1-users.cjs');

function seniorityLevel(object){
    let seniorsArray=[],internArray=[],juniorArray=[];

    for(let key in object){
        if((object[key].desgination).includes("Senior")){
            seniorsArray.push(object[key])
        }else if((object[key].desgination).includes("Intern")){
            internArray.push(object[key])
        }else{
            juniorArray.push(object[key])
        }
    }

    let senior=seniorsArray.sort((a,b)=>b.age-a.age)

    let junior=juniorArray.sort((a,b)=>b.age-a.age)

    let intern=internArray.sort((a,b)=>b.age-a.age)

    return [...senior,...junior,...intern];
}

let result=seniorityLevel(givenData);
console.log(result)

