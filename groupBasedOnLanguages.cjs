let givenData=require('./1-users.cjs')

function user(givenData){

 let groupData=Object.entries(givenData).reduce((prev, curr)=>{
    const desig=curr[1].desgination.split(" ").find((desgination)=>{
        return ['Golang','Javascript','Python'].includes(desgination);
    });

    if(prev[desig]===undefined){
        prev[desig] = [curr];
    }
    else{
        prev[desig].push(curr);
    }
    return prev;
 },{});
 return groupData;
}

let result=user(givenData);
console.log(result);
